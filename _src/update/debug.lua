function debugPortals()
	str = ""

	for key,value in pairs(map.Portals) do
		local stats = (map.Portals[key]["sender"] and "sender" or "-") .. ", "
		.. "self: " .. key .. ", pair: " .. map.Portals[key]["pairID"] .. "|" .. #map.Portals

		str  = str .. stats .. "\n"
	end

	love.graphics.print(str, 16, 16)
end

function debugDoors()
	str = ""

	for key,value in pairs(map.Doors) do
        local stats = 
        "self: " .. key .. 
        ", solver: " .. map.Doors[key]["Solve_X"] .. ", " .. map.Doors[key]["Solve_Y"] .. 
        ", sprite: " .. map.Doors[key]["spriteID"] ..
        "|" .. #map.Doors

		str  = str .. stats .. "\n"
	end

	love.graphics.print(str, 16, 16)
end