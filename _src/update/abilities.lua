--into update()
function tileLineWithCollision(layer, value, x, y, xDelta, yDelta)
	if isSolid(x+xDelta, y+yDelta) then
		return
	end

	i, j = xDelta, yDelta

	repeat
		layer[x+i][y+j] = value
		i = i+xDelta
		j = j+yDelta
	until isSolid(x+i, y+j)
end

function BresenhamLine(layer, value, x, y, targetX, targetY)
	local initStartVal = layer[x][y]
	local initTargetVal = layer[targetX][targetY]
	local initIterVal = EMPTY

	local startX, startY = tileToScreen(x, y)
	local destX, destY = tileToScreen(targetX, targetY)

	local diffX = math.abs(destX - startX)
	local diffY = math.abs(destY - startY)

	local sx = startX < destX and 1 or -1
	local sy = startY < destY and 1 or -1
	local err = diffX - diffY

	local currentX = startX
	local currentY = startY

	repeat
		-- get iteration tile, store its value and set it to the input
		tx, ty = screenToTile(currentX, currentY)
		initIterVal = layer[tx][ty]
		layer[tx][ty] = value

		local tmpErr = 2 * err

		if tmpErr > 0 - diffY then
			err = err - diffY
			currentX = currentX + sx
		end

		if tmpErr < diffX then
			err = err + diffX
			currentY = currentY + sy
		end
	until not map.isTileWithinBoundaries(tx, ty) 
		or isSolid(tx, ty) 
		or (tx == targetX and ty == targetY)

	-- reset every other tile involved
	layer[tx][ty] = initIterVal
	layer[x][y] = initStartVal
	layer[targetX][targetY] = initTargetVal
end

function createLaserBeam(sourceX, sourceY, dir)
	if dir == 'left' and not isSolid(sourceX-1, sourceY) then
		tileLineWithCollision(map.Laser, LASER_VER, sourceX, sourceY, -1, 0)
	elseif dir =='right' and not isSolid(sourceX+1, sourceY) then
		tileLineWithCollision(map.Laser, LASER_VER, sourceX, sourceY, 1, 0)
	elseif dir =='up' and not isSolid(sourceX, sourceY-1) then
		tileLineWithCollision(map.Laser, LASER_HOR, sourceX, sourceY, 0, -1)	
	elseif dir =='down' and not isSolid(sourceX, sourceY+1) then
		tileLineWithCollision(map.Laser, LASER_HOR, sourceX, sourceY, 0, 1)	
	end	
end