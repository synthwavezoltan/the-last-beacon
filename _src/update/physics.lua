-- lookup table of solid tiles on the tileset
-- collision detection is applies only on these values
SolidTiles = {
    ROCK, 
    RUIN_START, 
    4, 
    5, 
    6, 
    7, 
    MIRROR.LD, 
    MIRROR.LU, 
    MIRROR.RU, 
    MIRROR.RD, 
    PRISM.D, 
    PRISM.L, 
    PRISM.U, 
    PRISM.R, 
    17, 
    18, 
    DOOR.CLOSED, 
    23, 
    24, 
    PORTAL.PH1, 
    PORTAL.PH2, 
    30, 
    39, 
    ROCK_BEAM.L, 
    ROCK_BEAM.H, 
    ROCK_BEAM.R,
    ROCK_BEAM.U, 
    ROCK_BEAM.V, 
    ROCK_BEAM.D 
}

function isSolid(x, y)
	return isTileSolid(map.field[x][y])
end

function isTileSolid(Tile_Number)
	for i=1, #SolidTiles do
		if Tile_Number == SolidTiles[i] then
			return true
		end
	end
	
	return false
end

function hasLaser(x, y)
	return map.Laser[x][y] == LASER_VER or map.Laser[x][y] == LASER_HOR
end

-- checks if a given tile is hit by a laser
function hasIncomingLaser(x, y)
    return (x < SIZE_X and map.Laser[x+1][y] == LASER_VER)
        or (x > 0 and map.Laser[x-1][y] == LASER_VER)
        or (y < SIZE_Y and map.Laser[x][y+1] == LASER_HOR)
        or (y > 0 and map.Laser[x][y-1] == LASER_HOR)
end

--for tile 3-7; call this little boy in love.draw, at the check of each tiles
function calculateDamage(x,y, force)
	local val = map.field[x][y]
	local dmg = map.DamageCounter[x][y]

	local ruinState = val - RUIN_START + 1

	if map.isBreakableTile(x, y) then
		dmg = dmg + 1

		unit = 30/force
		threshold = ruinState*unit
		errorMargin = unit/2 -- to counter the errors caused by dmg == threshold

		if dmg >= threshold and dmg <= threshold + errorMargin  then
			map.field[x][y] = val +1
		end

		map.DamageCounter[x][y] = dmg
	elseif map.isBrokenTile(x, y) and dmg ~= 0 then 
		map.DamageCounter[x][y] = 0
	end
end

--for tile 9-12: mirrors
function checkMirrors(x,y)
	-- todo: lasers now reflected towards player
	local mirror = map.field[x][y]

	--left-down mirror
	if mirror == MIRROR.LD then
		if hasLaser(x-1, y) then
			createLaserBeam(x, y, 'down')
		end
		if hasLaser(x, y+1) then
			createLaserBeam(x, y, 'left')
		end
	--left-up
	elseif mirror == MIRROR.LU then
		if hasLaser(x-1, y) then
			createLaserBeam(x, y, 'up')
		end
		if hasLaser(x, y-1) then
			createLaserBeam(x, y, 'left')	
		end	
	--right-up
	elseif mirror == MIRROR.RU then
		if hasLaser(x+1, y) then
			createLaserBeam(x, y, 'up')
		end
		if hasLaser(x, y-1) then
			createLaserBeam(x, y, 'right')
		end		
	--right-down
	elseif mirror == MIRROR.RD then
		if hasLaser(x+1, y) then
			createLaserBeam(x, y, 'down')
		end
		if hasLaser(x, y+1) then
			createLaserBeam(x, y, 'right')
		end
	end
end


-- tile 13-16: prisms
function Prism_Behaviour(x,y)
	local mirror = map.field[x][y]

	--down-input, up-input
	if (mirror == PRISM.D and map.Laser[x][y+1] == LASER_HOR) or (mirror == PRISM.U and map.Laser[x][y-1] == LASER_HOR) then
		createLaserBeam(x, y, 'left')		
		createLaserBeam(x, y, 'right')				
	--left-input, right-input
	elseif (mirror == PRISM.L and map.Laser[x-1][y] == LASER_VER) or (mirror == PRISM.R and map.Laser[x+1][y] == LASER_VER) then
		createLaserBeam(x, y, 'up')		
		createLaserBeam(x, y, 'down')		
	end
end