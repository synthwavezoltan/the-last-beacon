function autotileLayerTile(x, y, lookup)
	if x < 2 or y < 2 or x == SIZE_X or y == SIZE_Y then
		return
	end

	local value = map.field[x][y]

	-- drop out aliases
	if inTable(value, lookup.aliases) then
		return
	end

	-- drop out if not in whitelist
	if #lookup.allowed ~= 0 and not inTable(value, lookup.allowed) then
		return
	end

	-- drop out blacklisted
	if inTable(value, lookup.blacklist) then
		return
	end

	local absoluteX = OFFSET_X+(x-1)*TILE_SIZE
	local absoluteY = OFFSET_Y+(y-1)*TILE_SIZE

	if inTable(map.field[x-1][y], lookup.aliases) then
		love.graphics.draw(lookup.tileset, Autoset[4], absoluteX, absoluteY)	
	end
	if inTable(map.field[x+1][y], lookup.aliases) then
		love.graphics.draw(lookup.tileset, Autoset[3], absoluteX, absoluteY)	
	end
	if inTable(map.field[x][y-1], lookup.aliases) then
		love.graphics.draw(lookup.tileset, Autoset[2], absoluteX, absoluteY)	
	end
	if inTable(map.field[x][y+1], lookup.aliases) then
		love.graphics.draw(lookup.tileset, Autoset[1], absoluteX, absoluteY)	
	end	

end

function AutoDetailer()
	for i=1, 5 do
		local x = math.random(2,SIZE_X-1)
		local y = math.random(2,SIZE_Y-1)
		
		if map.field[x][y] == ROCK then map.field[x][y] = 29 end			
		if map.field[x][y] == ROCK_BG then map.field[x][y] = 29 end		
		if map.field[x][y] == RUIN_START then map.field[x][y] = math.random (RUIN_START, RUIN_END -1) end
		
		if map.field[x][y] == 39 then 
		
			if math.random(100)%2 ==0 then
				if not isSolid(x-1, y) then map.field[x-1][y] = 29 end
				if not isSolid(x+1, y) then map.field[x+1][y] = 29 end
				if not isSolid(x, y-1) then map.field[x][y-1] = 29 end
				if not isSolid(x, y+1) then map.field[x][y+1] = 29 end
			else
				if isSolid(x, y+1) and map.field[x][y+1]>8 then map.field[x-1][y] = 48 end
			end
		end	
		
		if not map.field[x][y] == 29 then map.field[x][y] = 40 end	


	end

end



--function for texture animations
function animateRelativeTiles(j, i)
end

--function for texture animations
function animateFixTiles()
	--animated mark of the aim to reach with your laser	
	if map.isProperMap() then
		if game_clock%50 < 25 then 
			map.field[BEACON][2] = 17 
			else map.field[BEACON][2] = 18 
		end
	end
end

--what should happen when you win
function applyVictoryShutter()
	if map.id == GAME_LENGTH then
		GLOBAL_STATE = "ENDING"
	else
		GLOBAL_STATE = "PHASE_OUT"
	end

	local shutterInvert = math.floor(100/settings.victoryShutterSpeed)
	local shutterClock = game_clock % shutterInvert

	if shutterClock == 0 then
		if yOffsetAtVictory <=SIZE_Y then 
			yOffsetAtVictory = yOffsetAtVictory +1
		else
			yOffsetAtVictory = 0
			map.loadNext()	
		end
	end
end

function applySettings()
	love.audio.setVolume(settings.volume / 100)
end