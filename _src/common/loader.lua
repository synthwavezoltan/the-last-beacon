require '_src/common/config'

function newImageAbstract(name, format, path, filterMode)
    local img = love.graphics.newImage(path .. "/" .. name .. "." .. format)
    img:setFilter(filterMode, filterMode, ANISOTROPIC_FILTER)

    return img
end

function newImage(name)
    return newImageAbstract(name, "png", IMG_PATH, DEFAULT_FILTER_MODE)
end

-- love.graphics.newQuad wrapper simplified for in-game needs.
function newTileQuad(x, y, width, height, isAbsolutePx)
	if isAbsolutePx then
		width = math.floor(width / PIXEL_SIZE)
		height = math.floor(height / PIXEL_SIZE)
	end

	return love.graphics.newQuad(
		x*TILE_SIZE, y*TILE_SIZE, 
		TILE_SIZE, TILE_SIZE, 
		width*TILE_SIZE, height*TILE_SIZE)
end

function newTileset(tilesetImage, quadContainer)
    tilesetImage:setFilter(DEFAULT_FILTER_MODE, DEFAULT_FILTER_MODE, ANISOTROPIC_FILTER)

	local width = tilesetImage:getWidth()
	local height = tilesetImage:getHeight()
	local widthInPx = math.floor(width / PIXEL_SIZE)
	local heightInPx = math.floor(height / PIXEL_SIZE)

	for j = 0, heightInPx - 1 do
		for i = 0, widthInPx - 1 do
			quadContainer[#quadContainer+1] = newTileQuad(i, j, widthInPx, heightInPx, false)
		end
	end	
end

function newTilesetFromName(name, quadContainer)
    local image = newImage(name)

    newTileset(image, quadContainer)
end