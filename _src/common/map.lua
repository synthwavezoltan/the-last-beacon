require '_src/common/config'
require '_src/common/helpers'

function MapAbstract()
    local self = {}

    self.id = id or EDITOR_MAP
    self.state = MAP_STATES.UNLOADED
    self.field = {}

	function self.loadTheLastBeaconFormat(id)
		require(MAP_PATH .. "/" .. MAP_PREFIX .. id)

        return Field
	end

	-- create a full empty map and redefine its content
	function self.loadFromFile(id)
        print("Abstract class, not implemented")
        print("load can be abstracted but loadFromFile can't, as calling it from")
        print("this parent class can lead to issues. Thus, a pseudo-adapter is")
        print("created, in the form of `loadTheLastBeaconFormat` above.")
	end

	function self.load(id)
		print("load -- Abstract class, not implemented")
	end

	function self.createEmpty()
		print("createEmpty -- Abstract class, not implemented")
	end

	--create only single layer
	function self.createEmptyLayer(addBorder, defaultVal)

		local l = {}

		for x = 0, SIZE_X do
			l[x] = {}

			for y = 1, SIZE_Y do
				l[x][y] = (self.isBorderTile(x, y) and addBorder) and 1 or defaultVal
			end
		end

		return l
	end

	function self.isCriteriaMet()
		print("isCriteriaMet -- Abstract class, not implemented")
		return true
	end

	function self.isInProgress()
		return not self.isFinished() and GLOBAL_STATE ~= "ENDING"
	end

	function self.isFinished()
		-- todo: is it alright?
		return self.isCriteriaMet() or self.state == MAP_STATES.DONE
	end

	-- it's a playable, proper map
	function self.isProperMap()
		--print("isProperMap -- Abstract class, not implemented")
		return true
	end

	function self.isBorderTile(x,y)
		return x == 1 or y == 1 or x ==SIZE_X or y ==SIZE_Y
	end

	function self.isTileWithinBoundaries(x,y)
		return x >= 1 and y >= 1 and x <=SIZE_X and y <=SIZE_Y
	end

	-- resets a map without altering its original state
	function self.reset()
		print("reset -- Abstract class, not implemented")
    end

	return self
end
