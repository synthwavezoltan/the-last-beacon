function WorldObject(x, y)
	local self = {
		X = x,
		Y = y,
		hasGravity = false,
	}

	function self.animate()
		print("animate -- Abstract class, not implemented")
	end

	function self.resetState()
		print("resetState -- Abstract class, not implemented")
	end

	function self.canMove()
		print("canMove -- Abstract class, not implemented")
	end

	function self.isOnTile(x, y)
		return self.X == x and self.Y == y
	end

	function self.isFalling()
		return not isSolid(self.X, self.Y+1)
	end

	function self.applyGravity()
		if self.hasGravity and self.isFalling() then
			self.Y = self.Y +1
		end
	end

	return self
end