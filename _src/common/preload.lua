--[[
----------------------
            COMPUTED GLOBALS
----------------------
]]--

--[[
    Side length of a single tile in absolute pixels.
]]--
TILE_SIZE = TILE_RESOLUTION * PIXEL_SCALE    

--[[
    Size of an in-game pixel compared to the real display pixel.
]]--
PIXEL_SIZE = TILE_SIZE / PIXEL_SCALE

--[[
----------------------
            COMMON GLOBALS
----------------------
]]--
menu = SUB_MENU.MAIN
Minutes, Seconds = 0,0
Selected_Tile = 1

MouseX, MouseY, MouseTileX, MouseTileY = 0,0,1,1

game_clock = 0