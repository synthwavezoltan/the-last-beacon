require '_src/common/config'

function HasNeighbour(x,y, valueToCheck, Table)

	local itHas = false

	if Table[x-1][y] == valueToCheck then itHas = true
	elseif Table[x+1][y] == valueToCheck then itHas = true
	elseif Table[x][y-1] == valueToCheck then itHas = true
	elseif Table[x][y+1] == valueToCheck then itHas = true
	else itHas = false end
	
	return itHas
end

function inTable (val, tab)
    for index, value in ipairs(tab) do
        if value == val then
            return true
        end
    end

    return false
end

function screenToTile (x, y)
	local tileX = 1 + math.floor((x - OFFSET_X) / TILE_SIZE)
	local tileY = 1 + math.floor((y - OFFSET_Y) / TILE_SIZE)

	return tileX, tileY
end

function tileToScreen (tileX, tileY)
	local x = OFFSET_X + (( tileX - 1) * TILE_SIZE )
	local y = OFFSET_Y + (( tileY - 1) * TILE_SIZE )

	return x, y
end

function resetTimers()
	Minutes, Seconds = 0,0
	game_clock = 0
end

function contains(table, value)
	for index, val in ipairs(table) do
        if val == value then
            return true
        end
    end

	return false
end