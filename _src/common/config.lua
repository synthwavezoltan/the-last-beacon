--[[
----------------------
            SCREEN
----------------------
]]--

--[[
    Size of an unstretched tile, in pixels.
    Every tile must be a square; TILE_RESOLUTION is effectively its side
    length. This value forms the base of many other computations.
]]--
TILE_RESOLUTION = 8

--[[
    Pixelart stretching ratio.
    In order to avoid the use of unnecessarily large image files, and to
    ease working with pixelarts, every image file is loaded in its original
    size, and then stretched by the engine. PIXEL_SCALE specifies this
    streching ratio.
]]--
PIXEL_SCALE = 4

--[[
    Dimensions of the map, expressed in number of tiles.
    By default, this means the entire visible playing area.
]]--
SIZE_X,SIZE_Y = 16, 16

--[[
    Camera size and offset.
    <todo> Currently not in use; reserved for future camera handling
]]--
SCREEN_SIZE_X, SCREEN_SIZE_Y = 16, 16
SCREEN_OFFSET_X, SCREEN_OFFSET_Y = 0, 0

--[[
    Window dimensions for easy reach. Configured in conf.lua, obviously.
]]--
WIDTH, HEIGHT = 800, 600

--[[
    On-screen offset of playing area from the top-left corner.
]]--
OFFSET_X, OFFSET_Y = 0, 0


--[[
----------------------
            FILES
----------------------
]]--

--[[
    Location of folder containing all tilesets and graphics to load.
    Relative to root.
]]--
IMG_PATH = "Gfx"

--[[
    Location and name pattern to all loadable maps.
    Relative to root.
]]--
MAP_PATH = 'Maps'
MAP_PREFIX = 'map'

--[[
----------------------
            IMAGES
----------------------
]]--

--[[
    Image filter mode to use when scaling up images.
]]--
DEFAULT_FILTER_MODE = "nearest"
ANISOTROPIC_FILTER = 0

--[[
    Default extension of images loaded and used.
]]--
DEFAULT_IMAGE_FORMAT = "png"

--[[
    File names of graphics to be loaded on start.
]]--
GRAPHICS = {}


--[[
----------------------
            GAME
----------------------
]]--

--[[
    The intended number of levels in story mode.
    Unless scripted or specified otherwise, when the level with this number
    is reached, the game sets its state to ENDING automatically.
]]--
GAME_LENGTH = 0

--[[
    Identifier of the map "slot" intended for editor mode.
]]--
EDITOR_MAP = 0

--[[
    Most basic GLOBAL_STATE options and their meanings:
    * MENU          Menu is open; intro finished or skipped.
    * MAP           Regular playing mode.
    * EDITOR        Map editor is open.
    * PHASE_OUT     Victory condition achieved.
    * ENDING        The last map is finished.
]]--
GLOBAL_STATE = "MENU"

-- menu
SUB_MENU = {
    MAIN    = 'main',
    CREDITS = 'credits'
}

-- map
MAP_STATES = {
    UNLOADED    = 'unloaded',
	IN_PROGRESS	= 'inProgress',
	DONE 		= 'done'
}