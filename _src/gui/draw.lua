
require '_src/gui/texts'

function drawRelative(image, naturalX, naturalY)
    love.graphics.draw(image, naturalX * PIXEL_SCALE, naturalY * PIXEL_SCALE, 0, PIXEL_SCALE, PIXEL_SCALE)
end

function printRelative(text, naturalX, naturalY)
    love.graphics.print(text, naturalX * PIXEL_SCALE, naturalY * PIXEL_SCALE)
end

--[[
    LAYER 1: SKY AND BACKGROUND
]]--
function drawBackground()
	if menu ~= SUB_MENU.CREDITS then
		love.graphics.draw(images[1], 0, yOffsetAtStart/4, 0, PIXEL_SCALE, PIXEL_SCALE)	
	end

	if map.state ~= MAP_STATES.UNLOADED then
		love.graphics.draw(images[9], OFFSET_X, OFFSET_Y, 0, PIXEL_SCALE, PIXEL_SCALE)
	end
end

-- starting screen
function drawMainMenu() 
    --middle border in main menu
    drawRelative(images[4], 0, (yOffsetAtStart/4*1.5 + OFFSET_Y/PIXEL_SCALE))
    
    --titlepic
    drawRelative(images[6], 52, (yOffsetAtStart+104)*0.5)       

    love.graphics.print("Zoltan Schmidt presents...",130*1.5,2*yOffsetAtStart+600*1.5)	

    if yOffsetAtStart == 0 then
        printRelative(openingText, 52, 100)
        printRelative(creditsOld .. "\n" .. creditsNew, 40, 170)				
    else
        printRelative("(esc to skip)",210, 186.6)	
    end

end

function drawCredits()
    local scrollVal =  (800 - game_clock)

    --middle border in main menu
    drawRelative(images[4], 0, (yOffsetAtStart/4+88*1.5)/PIXEL_SCALE) 
   
    --love.graphics.draw(images[5], -3.3*PIXEL_SCALE ,2*yOffsetAtStart+500*1.5, 0, PIXEL_SCALE/3, PIXEL_SCALE/3)	--logo
    love.graphics.draw(images[7], 110*PIXEL_SCALE, 34*PIXEL_SCALE, 0, PIXEL_SCALE/3, PIXEL_SCALE/3)		    --game logo
    printRelative(creditsScroll, scrollVal/3, 15)
    printRelative(creditsOld .. "\n" .. creditsNew, 40, 170)	
end

--
function drawHUD()
    --HUD elements: left & right borders
    love.graphics.draw(images[2], 0*1.5, OFFSET_Y, 0, PIXEL_SCALE, PIXEL_SCALE)
    love.graphics.draw(images[3], (OFFSET_X + (TILE_SIZE *SIZE_X)), OFFSET_Y, 0, PIXEL_SCALE, PIXEL_SCALE)

    --game progress "bar"
    --[[
    for i=1, 3 do
        for j=1, 5 do
            -- my first ternary operator-like syntax in lua
            local levelReached = ((3*(j-1))+ i <= map.id and 1 or 0)

            love.graphics.draw(hudImage, hudIcons[57 + levelReached], i*(TILE_SIZE+12) - 7*PIXEL_SCALE ,j*(TILE_SIZE+12) + 90*PIXEL_SCALE)	
        end
    end
    ]]--

    -- show, what elements are used on the map
    if countRuins(map.field) > 0 
        then love.graphics.draw(hudImage, hudIcons[FEATURED_ON_MAP.RUINS], 675*1.5, 220*1.5) end
    if countMirrors(map.field) > 0 
        then love.graphics.draw(hudImage, hudIcons[FEATURED_ON_MAP.MIRROR], 717*1.5, 220*1.5) end	
    if countPrisms(map.field) > 0  
        then love.graphics.draw(hudImage, hudIcons[FEATURED_ON_MAP.PRISM], 760*1.5, 220*1.5) end	

    if #map.Doors ~= 0 then love.graphics.draw(hudImage, hudIcons[FEATURED_ON_MAP.DOOR], 675*1.5, 250*1.5) end	
    if #map.Portals ~= 0 then love.graphics.draw(hudImage, hudIcons[FEATURED_ON_MAP.PORTAL], 760*1.5, 250*1.5) end

    --showing used abilities
    if player.hook.active then love.graphics.draw(hudImage, hudIcons[ACTIVE_ABILITIES.HOOK], 675*1.5, 151*1.5) end
    if player.laser.active then love.graphics.draw(hudImage, hudIcons[ACTIVE_ABILITIES.LASER], 760*1.5, 151*1.5) end

    --passed time:
    local clockMin = (Minutes < 10 and '0' .. Minutes or Minutes)
    local clockSec = (Seconds < 10 and '0' .. Seconds or Seconds)
    
    printRelative(clockMin..":"..clockSec,10,85)
    
    --level text
    if map.id ~= EDITOR_MAP and map.id < GAME_LENGTH then
        printRelative(sidebarTexts[map.id], 226, 96)
    end

    -- tile selector for editor menu
    if GLOBAL_STATE == "EDITOR" then
        for i=-10, 10 do
            local tileToShow = Selected_Tile - i

            if tileToShow > 0 and tileToShow < #Tileset then
                drawQuad(tileToShow, 230*PIXEL_SCALE, (110*PIXEL_SCALE) + (i * TILE_SIZE))
            end
        end

        drawDoorHelper()

        love.graphics.rectangle("line", 230*PIXEL_SCALE, 110*PIXEL_SCALE, TILE_SIZE, TILE_SIZE)
    end
end

-- draw line for helping door editing
function drawDoorHelper()
    if GLOBAL_STATE == "EDITOR"
        and Selected_Tile == LOCK.CLOSED 
        and #map.Doors > 0
        and map.Doors[#map.Doors].isLockMissing() 
    then
        local latestDoor = map.Doors[#map.Doors]
        local doorPosX, doorPosY = tileToScreen(latestDoor.X,latestDoor.Y)
        love.graphics.line(doorPosX + (TILE_SIZE/2), doorPosY + (TILE_SIZE/2), MouseX, MouseY)
    end
end

function drawBeaconArtifacts()
    for i=1, 4 do
        drawQuad(LASER_HOR, OFFSET_X + ((BEACON-1)*TILE_SIZE), OFFSET_Y-(i*TILE_SIZE))
    end
    map.Laser[BEACON][1] = LASER_HOR
    map.Laser[BEACON][2] = LASER_HOR	
end

function countRuins(Table)
	return 
		countTiles(Table, RUIN_START) 
		+ countTiles(Table, 4) 
		+ countTiles(Table, 5) 
        + countTiles(Table, 6)
        + countTiles(Table, 7)
end

function countMirrors(Table)
	return 
		countTiles(Table, MIRROR.LD) 
		+ countTiles(Table, MIRROR.LU) 
		+ countTiles(Table, MIRROR.RU) 
		+ countTiles(Table, MIRROR.RD)
end

function countPrisms(Table)
	return 
		countTiles(Table, PRISM.D) 
		+ countTiles(Table, PRISM.L) 
		+ countTiles(Table,  PRISM.U) 
		+ countTiles(Table,  PRISM.R)
end

function countTiles(Table, valueToCheck)
	local count = 0
	for j=2, SIZE_X-1 do
		for i=2,SIZE_Y-1 do
			if Table[j][i] == valueToCheck then count = count + 1 end
		end
	end
	
	return count
end