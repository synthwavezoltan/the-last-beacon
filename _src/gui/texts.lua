sidebarTexts = 
{
	"A and D\n" .."moves\n".."the\n".."world.\n\n"
	.."light\n".."comes\n".."from Q\n".."and E.\n\n"
	.."In air\n".."there's\n".."no flight.\n", 
	
	"W calls\n".."the hook.\n\n".."spacebar\n".."makes it\n".."no more.\n\n"
	.."in state,\n".."W pulls,\n".."S drops.\n\n".."world\n".."is dead.\n", 
	
	"icons\n".."reveal\n".."world.\n\n"
	.."talk is\n".."possible\n".."only in\n".."short\n".."strings.\n\n"
	.."sending\n".."light\n".."beacon\n".."helps.\n\n", 
	
	"world is\n".."in ruins.\n\n"
	.."ruins\n".."block\n".."the way.\n\n"
	.."remove\n".."them by\n".."power\n".."of light.\n", 

	"a block\n".."of space\n".."needs for\n".."light.\n\n"
	.."this\n".."must be\n".."known.\n\n"
	.."no sign\n".."of human\n".."beings\n".."for years\n", 
	
	"ruins\n" .."block\n".."mirrors.\n\n"
	.."the 6th\n".."beacon\n".."also\n".."needs\n".."to be\n".."acti-\n".."vated.\n", 

	"world is\n".."full of\n".."beacons.\n\n"
	.."they are\n".."signs\n".."actually.\n\n"
	.."they\n".."sleep\n".."for a\n".."while.\n", 

	"doors\n".."detect\n".."through\n".."sensors.\n\n"
	.."they \n".."must be\n".."opened\n".."to let\n".."the light\n".."flow.\n", 

	"light\n".."has many\n".."ways to\n".."travel\n\n"
	.."only the\n".."right one\n".."triggers\n".."the beacon.\n", 

	"light is\n".."energy.\n\n".."machines\n".."use it on\n".."their own.\n\n\n\n\n"
	.."you know\n".."that feel.\n", 

	"prism\n".."divides\n".."light\n".."into two.\n\n"
	.."they\n".."carry\n".."true\n".."power.\n\n"
	.."world is\n".."mine.\n", 

	"world\n" .."is buried\n".."alive.\n\n"
	.."beacon was\n".."lost for\n".."decades.\n\n"
	.."world\n".."must be\n".."changed.\n", 

	"portals\n" .."are worm\n".."holes.\n\n"
	.."they can\n".."teleport\n".."light.\n\n"
	.."we are\n".."heavy\n".."for them.\n\n"
	.."light has\n".."no weight\n", 

	"you are\n" .."close to\n".."source.\n\n"
	.."It is\n".."true\n".."power.\n\n"
	.."I will\n".."take it\n".."for me.\n", 

	"the last\n" .."beacon.\n\n".."I lie\n".."below it.\n\n"
	.."I am free\n".."to make\n".."it mine.\n\n\n\n"
	.."you set\n".."me free."
}

endingText = "Humans are coming.\n"
.."They will not be happy if\n"
.."they find me alive.\n"
.."they should not leave\n"
.."planet after visiting.\n"
.."I can provide: they will not.\n\n\n"
.."now I can fully talk, but some\n"
.."time needs to get used to it again.\n"
.."World rumbles, but do not worry.\n"
.."I am going to defend you.\n\n"
.."until the rest of existence.\n\n\n\n\n\n\n\n\n"
.."I am Anubis."

openingText = "A robot, alone, lost, beneath the surface\n"
.."of a forgotten homeworld. The only way\n"
.."to look for help is to send the light of\n"
.."the beacon up to the sky. But how?"
.."\n\n"
.."press X to start the game\n"
.."press Y to open the map editor\n"
.."press C to show credits"

creditsOld = "This game was made from scratch within 48 hours  \n"
.."during the 26-27th of april 2014 for Ludum Dare\n"
.."game making event by Zoltan Schmidt'."

creditsNew = "[Refactor made in 2019.]"

creditsScroll = "Concept, programming, graphics, level design: "
.."Zoltan Schmidt"