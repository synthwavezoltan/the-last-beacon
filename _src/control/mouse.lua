require '_src/objects/door'
require '_src/objects/portal'
require '_src/editor/map'

function love.mousepressed(x,y,button)
	--mouse actions for editing mode.
	if GLOBAL_STATE == "EDITOR" then
		if button == 1 then
			if map.isTileWithinBoundaries(MouseTileX, MouseTileY) then
				--door door
				if Selected_Tile == DOOR.CLOSED then
					Door(MouseTileX, MouseTileY, -1, -1)
				--door lock
				elseif Selected_Tile == LOCK.CLOSED then
					latestDoor = map.Doors[#map.Doors]
					latestDoor.Solve_X = MouseTileX
					latestDoor.Solve_Y = MouseTileY	
				--portal
				elseif Selected_Tile == PORTAL.PH1 or Selected_Tile == PORTAL.PH2 then
					-- if adding a portal would make the portal count even
					-- then the pair is tentatively set to the previous teleport
					-- set to the next portal otherwise
					portalCount = #map.Portals

					if (portalCount) %2 == 1 then
						Portal(MouseTileX, MouseTileY, portalCount)
					elseif (portalCount) %2 == 0 then
						Portal(MouseTileX, MouseTileY, portalCount+2)
					end
				--all else: place tiles
				else
					map.field[MouseTileX][MouseTileY] = Selected_Tile
				end
			end
		end	

		-- saves map with a unix timestamp
		if button == 2 then
			SaveMap(os.time(os.date("!*t")))
		end
	end	
end

function love.wheelmoved(x, y)
	--mouse actions for editing mode.
	if GLOBAL_STATE == "EDITOR" then
		if y > 0 and Selected_Tile < #Tileset then 
			Selected_Tile = Selected_Tile + 1 
		elseif y < 0 and Selected_Tile > 1 then 
			Selected_Tile = Selected_Tile - 1
		end
	end
end

function updateMouse()
	--mouse constants
	MouseX, MouseY = love.mouse.getX(), love.mouse.getY()
	MouseTileX, MouseTileY = screenToTile(MouseX, MouseY)

	--editing mode 
	if GLOBAL_STATE == "EDITOR" then
		if love.mouse.isDown(1) then 
			map.field[MouseTileX][MouseTileY] = Selected_Tile 
		end
	end
end