function love.keypressed(key)
	if GLOBAL_STATE == "INTRO" then

	elseif GLOBAL_STATE == "MENU" then

	elseif GLOBAL_STATE == "MAP" then
		if map.isInProgress() and map.isProperMap() then
			handlePlayerMovement(key)
		else
			-- miscellaneous key actions: only when map isn't in progress
		end
	elseif GLOBAL_STATE == "EDITOR" then
		if map.isInProgress() and map.isProperMap() then
			handlePlayerMovement(key)
		else
			-- miscellaneous key actions: only when map isn't in progress
		end

		if key == 't' then
			if map.isInProgress() then
				AutoDetailer()
			end
		end

	elseif GLOBAL_STATE == "PHASE_OUT" then

	elseif GLOBAL_STATE == "ENDING" then

	end


	
	--quick exit	
	if key == 'escape' then
		if GLOBAL_STATE == "INTRO" then
			GLOBAL_STATE = "MENU"
			yOffsetAtStart = 0
		elseif GLOBAL_STATE == "PHASE_OUT" then
			yOffsetAtVictory = SIZE_Y + 1
		else
			love.event.push('quit')
		end
	end
	
    if key == 'x' then
		if GLOBAL_STATE == "MENU" and menu == SUB_MENU.MAIN then
			GLOBAL_STATE = "MAP"
			yOffsetAtStart = 0
            if debug.gotoLast then
                map.load(GAME_LENGTH)
            else
                map.load(1)
                love.audio.play(menuSound)
            end
        end
	end	

    if key == 'c' then
        if GLOBAL_STATE == "MENU" then
            if menu == SUB_MENU.CREDITS then
                menu = SUB_MENU.MAIN
            else
                menu = SUB_MENU.CREDITS 
            end
        end
	end	

	if key == 'y' then
		GLOBAL_STATE = "EDITOR"
		yOffsetAtStart = 0
		map.id = EDITOR_MAP
		map.createEmpty()
	end	

	-- last map cheat
	if key == 'k' then
		debug.gotoLast = not debug.gotoLast
	end

	-- damageCounter debug
	if key == 'l' then
		debug.displayDamage = not debug.displayDamage
	end	 
end

function handlePlayerMovement(key)
	if key == 'w' then
		if player.hook.active then
			if not isSolid(player.X, player.Y-1) then
				player.Y = player.Y-1
				map.Laser[player.X][player.Y] = EMPTY		
			end			
		else
			player.activateHook()
		end
	end
	
	if key == 's' then
		if player.canMoveDown() then 
			player.Y = player.Y+1
		end	
	end

	if key == 'a' then
		if player.canMoveLeft() then	
			player.X = player.X -1
		end
	end

	if key == 'd' then
		if player.canMoveRight() then						
			player.X = player.X +1
		end
	end	
	
	--activating laser
	if key == 'q' then
		if player.laser.active then
			player.turnOffLaser()
		else
			player.turnOnLaser('left')
		end
	end
	
	if key == 'e' then	
		if player.laser.active then
			player.turnOffLaser()
		else
			player.turnOnLaser('right')
		end
	end
   
   --deactivating stuff
	   if key == 'space' then
		--if hanging on rope, and laser is active, then turn off only the laser
		if player.hook.active then 
			if player.laser.active then
				player.turnOffLaser()
			else
				player.hook.active = false
				-- delete hook graphics
				tileLineWithCollision(map.Laser, EMPTY, player.X, player.Y, 0, -1)
			end
		else
		--but turn off laser even if its active, but hook isnt
			if player.laser.active then
				player.turnOffLaser()
			end		
		end
	end
end