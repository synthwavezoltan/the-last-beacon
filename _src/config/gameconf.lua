require '_src/common/config'

--[[
    Overwritten params, in alphabetical order.
]]--
PIXEL_SCALE = 4.5

GAME_LENGTH = 15

GRAPHICS = {
    'SKY1',
    'BORDER_LEFT',	
    'BORDER_RIGHT',	
    'BORDER_EMPTY',	
    'OLD_LOGO',	
    'TITLEPIC',	
    'LOGO_ALT',
    'HUD_TILESET_24',
    'BACKGROUND',
}

OFFSET_X, OFFSET_Y = PIXEL_SCALE*48, PIXEL_SCALE*28.9

SIZE_X,SIZE_Y = 21, 21

SCREEN_SIZE_X, SCREEN_SIZE_Y = 21, 21

SCREEN_OFFSET_X, SCREEN_OFFSET_Y = 0, 0



WIDTH, HEIGHT = 264*PIXEL_SCALE, 197*PIXEL_SCALE


--[[
    Options and their meanings in this game:
    * INTRO         The introduction sequence is in playing.
    * MENU          Menu is open; intro finished or skipped.
    * MAP           Regular playing mode.
    * EDITOR        Map editor is open.
    * PHASE_OUT     Victory condition achieved.
    * ENDING        The last map is finished.
]]--
GLOBAL_STATE = "INTRO"

--[[
    CONFIGURABLE VARIABLES
]]--

--[[
    Represents an "astract beacon"; that is, the X position of exit tile.
    Unless specified otherwise, the victory condition of every map is to
    have an upward laser beam reach the top of the tilemap at this X 
    position. Then, the intermission phase starts.
]]--
BEACON = 12

laserStrength = 1

settings = {
    volume  = 5, -- out of 100
    victoryShutterSpeed = 10, -- out of 100
}

debug = {
    displayDamage = false,  -- Mode in which damage counted on every tile is displayed.
    gotoLast = false,       -- Mode in which pressing X sends you to the last level.
    disableIntro = false, -- todo
    disablePhaseout = false, -- todo
}

--[[
    ACTUAL GAME VARIABLES
]]--
require 'const'

-- simply global vars
recalculateLasers = false
yOffsetAtStart = -600
yOffsetAtVictory = 0