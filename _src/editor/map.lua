-- in retrospective, I'm quite fucking proud I *actually* did it way back in 2014...
function SaveMap(mapnum)
	MapFile = love.filesystem.newFile("map" .. mapnum ..".lua")
	k = MapFile:open("w")
	
	--beginning
	--player
	MapFile:write("player.X="..player.X.."\r\n".."player.Y="..player.Y.."\r\n")

	--doors
	for i=1, #map.Doors do
		door = map.Doors[i]
		MapFile:write("Door("..door.X..","..door.Y..","..door.Solve_X..","..door.Solve_Y..")\r\n")
	end
	
	--portals
	for i=1, #map.Portals do
		MapFile:write("Portal("..map.Portals[i].X..","..map.Portals[i].Y..","..map.Portals[i].pairID..")\r\n")
	end
	
	--Field itself to the end
	MapFile:write("Field={\r\n")
	
	for j=1,SIZE_X do	
		MapFile:write("{")		
		for i=1,SIZE_Y do
			--if something that should be put down from the special tables
			--just write a "2" there
			if (map.field[j][i] >= LOCK.CLOSED and map.field[j][i] <= DOOR.OPEN)
			or map.field[j][i] == PORTAL.PH1 or map.field[j][i] == PORTAL.PH2 then
				MapFile:write(EMPTY .. ",")
			else
				MapFile:write(map.field[j][i] .. ",")				
			end
		end	
		
		if j ==SIZE_X
		then
			MapFile:write("},\r\n")
		else
			MapFile:write(",}\r\n")
		end
	end	
	MapFile:write( "\r\n}\r\n")
	
	--end		
	MapFile:close( )
end