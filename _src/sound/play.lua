function generateLaserSound()
	if player.laser.active then 
		love.audio.play(laser)
		if laser:isPlaying() 
			and game_clock%45 == 0 
		then 
			laser:seek(0, "seconds") 
		end	
	end
end