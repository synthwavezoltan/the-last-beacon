require '_src/common/gui'
require '_src/common/loader'

--
function loadFont()
    local fontSize = (16/3) * PIXEL_SCALE
    love.graphics.setFont(love.graphics.newFont("slkscre.ttf", fontSize))	
end

function loadGraphics()
    images = {}

	for key, imgName in pairs(GRAPHICS) do 
        images[#images+1] = newImage(imgName)
	end
end

--
function loadSounds()
    menuSound 	= love.audio.newSource("Sounds/menu.wav", "static")	
    laser 		= love.audio.newSource("Sounds/laser.wav", "static")	
end

-- loading every tileset
function loadTilesets()
	-- regular tileset
	TilesetPic = newImage('TILESET_24')
	Tileset = { }
	newTileset(TilesetPic, Tileset)

	-- autotiling
	AutoPic = newImage('AUTOTILE')
	Autoset = { }
	newTileset(AutoPic, Autoset)

	-- autotiling (second type)
	AutoPicBg = newImage('AUTOTILE_2')
	AutosetBg = { }
	newTileset(AutoPicBg, AutosetBg)

	-- HUD icons
	hudImage = newImage('HUD_TILESET_24')
	hudIcons = { }
	newTileset(hudImage, hudIcons)
end

function drawQuad(id, x, y)
	love.graphics.draw(TilesetPic, Tileset[id], x, y)
end

-- load player assets; essentially a tileset for now
function loadPlayerAssets()
	PlayerspriteIDs = newImage('PLAYER')
	PlayerCharset = { }
	newTileset(PlayerspriteIDs, PlayerCharset)
end