require '_src/common/map'
require '_src/objects/player'
require '_src/update/misc'
require '_src/update/physics'

function Map()
	local self = MapAbstract()

	self.Laser = {}
	self.DamageCounter = {}	

	self.Doors = {}
	self.Portals = {}

	-- create a full empty map and redefine its content
	function self.loadFromFile(id)
		local f = self.createEmpty()

		f = self.loadTheLastBeaconFormat(id)

		self.field = f
	end

	function self.load(id)
		self.preReset()

		self.id = id or 0

		if GLOBAL_STATE == "EDITOR" then
			return -- do nothing
		end

		if GLOBAL_STATE == "MAP" then
			if self.id > GAME_LENGTH then 
				GLOBAL_STATE = "ENDING"
				return
			end

			self.loadFromFile(self.id)
		end
	end

	function self.createEmpty()
		--creating layers
		self.field = self.createEmptyLayer(true, EMPTY)
		self.Laser = self.createEmptyLayer(false, EMPTY)
		self.DamageCounter = self.createEmptyLayer(false, 0)

		self.field[BEACON][1] = ROCK_BG
		self.field[BEACON][2] = EXIT
		self.field[BEACON][20] = MIRROR.RU
		
		--defining Player
		player = Player(17,20)

		-- door and portal containers
		self.Doors = {}
		self.Portals = {}	
	end

	--create only single layer
	function self.createDamageLayer()

		local l = {}

		for x = 0, SIZE_X do
			l[x] = {}

			for y = 1, SIZE_Y do
				l[x][y] = self.isBreakableTile(x,y) and 1 or defaultVal
			end
		end

		return l
	end

	function self.isCriteriaMet()
		return self.Laser[BEACON][3] == LASER_HOR
	end

	-- resets a map without altering its original state
	function self.reset()
		self.preReset()

		player.resetState()

		self.field[BEACON][1] = ROCK_BG

		-- door and portal containers
        for id, door in ipairs(map.Doors) do
            door.close()
		end
		-- todo ?
		--self.Portals = {}
	end

	function self.preReset()
		resetTimers()
		yOffsetAtVictory = 0
		self.state = MAP_STATES.IN_PROGRESS
	end
	
	function self.loadNext()
		if self.id == EDITOR_MAP then
			GLOBAL_STATE = "EDITOR"
			self.reset()
			return
		end

		if self.id == GAME_LENGTH then
			GLOBAL_STATE = "ENDING"
			-- todo: then what?
			return
		end

		GLOBAL_STATE = "MAP"

		self.load(self.id + 1)
	end
    
    -- tile 19-22: solving doors
    function self.checkDoors()
        for id, door in ipairs(self.Doors) do 
            door.check()
        end 
	end

	-- tile 25-26: portals
	function self.checkPortals()
		--drawing
        for id, portal in ipairs(self.Portals) do 
			portal.castBeam()
			portal.animate()
        end	
	end
	
	function self.isBreakableTile(x,y)
		local val = self.field[x][y]

		return val >= RUIN_START and val < RUIN_END
	end

	function self.isBrokenTile(x,y)
		local val = self.field[x][y]

		return val == RUIN_END
	end

	function self.drawGameplay()
		-- todo: better location for this
		autoTilingLookup = {
			foreground = {
				-- for these, apply the same autotiling textures
				aliases = {
					ROCK, ROCK_BEAM.L, ROCK_BEAM.H, ROCK_BEAM.R,
					53, ROCK_BEAM.U, ROCK_BEAM.V, ROCK_BEAM.D,
					39
				},
				-- the fields on which you can apply it
				allowed = {},
				-- obvious
				blacklist = {
					RUIN_START, 4, 5, 6, 7,
				},
				tileset = AutoPic,
			},
			background = {
				aliases = {
					ROCK_BG, RUIN_END, EXIT, 18, DOOR.CLOSED, DOOR.OPEN,
					PORTAL.PH1, PORTAL.PH2, 27, 28, 29,
					40, 47, 48
				},
				allowed = {
					EMPTY
				},
				blacklist = {},
				tileset = AutoPicBg,
			}
		}

		for j=1, SIZE_X do
			for i=1,SIZE_Y do
				local outsideShutter = SIZE_Y - i >= yOffsetAtVictory

				--drawing field itself
				if outsideShutter then
					-- basic tiles
					drawQuad(map.field[j][i],  tileToScreen(j, i))

					-- autotiling
					if map.isProperMap() then
						autotileLayerTile(j, i, autoTilingLookup.foreground)
						autotileLayerTile(j, i, autoTilingLookup.background)
					end
				end

				-- laser
				-- todo: should it be here?
				if hasLaser(j, i) and not player.laser.active then	
					map.Laser[j][i] = EMPTY
				end

				--drawing field itself AND Laser table
				if not map.isFinished() or outsideShutter then
					drawQuad(map.Laser[j][i], tileToScreen(j,i))
				end

				--damagecounter for debugging
				if debug.displayDamage then
					love.graphics.print(map.DamageCounter[j][i], tileToScreen(j, i))
				end
			end
		end

		player.draw()
	end

	function self.updateGameplay(dt)
        -- whether the level is done is decided here
        if self.isCriteriaMet() then
            self.state = MAP_STATES.DONE
		else			
            self.state = MAP_STATES.IN_PROGRESS
        end

		if self.isFinished() then
			applyVictoryShutter()
        else
			for j=1, SIZE_X do
				for i=1,SIZE_Y do
					if hasIncomingLaser(j,i) then
						checkMirrors(j,i)
						Prism_Behaviour(j,i)
						calculateDamage(j,i,laserStrength)
					end
					
					animateRelativeTiles(j,i)
				end
			end
	
			player.displayGrapplingHook()
	
			self.checkDoors()
			self.checkPortals()
	
			player.displayLaser()
	
			if game_clock%10 == 0 then 
				player.applyGravity()
			end
	
			--actual clock
			if game_clock%60 == 0 then 
				Seconds = Seconds + 1 
	
				if Seconds == 60 then 
					Minutes = Minutes + 1 
					Seconds = 0 
				end
			end	
		end

		player.animate()
		generateLaserSound()	--sound effect

	end


	return self
end
