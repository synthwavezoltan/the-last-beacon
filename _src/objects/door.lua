require '_src/common/objects/worldobj'

function Door(x, y, lockX, lockY)
	local self = WorldObject(x, y)

	-- position of door AND its lock
	self.Solve_X  = lockX
	self.Solve_Y  = lockY

	-- inner variables, storing state
	self.id		  = #map.Doors+1
	self.spriteID = DOOR.CLOSED

	function self._construct()
		map.Doors[self.id] = self
	end

	function self.close()
		self.spriteID = DOOR.CLOSED
		map.field[self.Solve_X][self.Solve_Y] = LOCK.CLOSED
	end

	function self.check()
		if self.isLockMissing() then
			return
		end

		--detecting
		if player.isOnTile(self.Solve_X, self.Solve_Y + 1) then		
			self.open()
		else
			self.close()
		end

		--drawing
		--todo: put this part of the logic elsewhere
		map.field[self.X][self.Y] = self.spriteID
	end

	function self.open()
		self.spriteID = DOOR.OPEN
		map.field[self.Solve_X][self.Solve_Y] = LOCK.OPEN
    end
    
    function self.isLockMissing()
        return self.Solve_X == -1 or self.Solve_Y == -1
    end

	self._construct()
	return
end