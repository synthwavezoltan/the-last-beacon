require '_src/common/objects/worldobj'

function Portal(x, y, pair)
	local self = WorldObject(x, y)

	-- inner variables, storing state
	self.id		 = #map.Portals+1
	self.pairID   = pair  -- todo: validate pairID
	self.sender   = false

	function self._construct()
		map.Portals[self.id] = self
	end

	function self.castBeam()
		if self.sender then
			return
		end

		local pairObj = map.Portals[self.pairID]
		local dir = nil

		if map.Laser[self.X+1][self.Y] == LASER_VER then
			dir = 'left'
		elseif map.Laser[self.X-1][self.Y] == LASER_VER then
			dir = 'right'
		elseif map.Laser[self.X][self.Y-1] == LASER_HOR then
			dir = 'down'
		elseif map.Laser[self.X][self.Y+1] == LASER_HOR then
			dir = 'up'
		end

		if dir == nil then
			self.sender = false

			return
		end

		createLaserBeam(pairObj.X , pairObj.Y , dir)	
		pairObj.sender = false
		self.sender = true
	end

	function self.animate()
		if game_clock%40 < 20 then 
			map.field[self.X][self.Y] = PORTAL.PH1
		else 
			map.field[self.X][self.Y] = PORTAL.PH2
		end
	end
	
	self._construct()
	return
end
