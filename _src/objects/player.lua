require '_src/common/objects/worldobj'

function Player(x, y)
	local self = WorldObject(x, y)

	-- override
	self.hasGravity = true

	-- configurable properties
	self.laserStrength = laserStrength
	-- hidden state properties
	self.spriteID = 1
	self.hook = {
		active = false,
	}
	self.laser = {
		active = false,
		direction = 'right',
	}

	function self._construct()
		self.resetState()
	end

	-- private fields are implemented using locals
	-- they are faster than table access, and are truly private, so the code that uses your class can't get them
	--local private_field = init

	--function self.foo()
	--	return self.public_field + private_field
	--end

	--function self.bar()
	--	private_field = private_field + 1
	--end

	function self.resetState()
		self.X = 17
		self.Y = 20

		self.deactivateHook()
		self.turnOffLaser()
	end

    function self.activateHook()
		--if I have place above my head and I don't use the laser at the same time
		if self.laser.active or self.hook.active then
			return
		end

        if isSolid(self.X, self.Y-1) then
			return
		end

		self.hook.active = true
	end

	function self.deactivateHook()
		self.hook.active = false
	end
	
	function self.canMove()
		return not self.laser.active
	end
    
	function self.canMoveLeft()
		--condition 1: be within boundaries
		--condition 2: try to step on a non-solid tile
		--condition 3: do not hang on that rope
		--condition 4: do not run around with laser damnit!
		return self.canMove()
			and not self.hook.active 
			and self.X > 0 
			and not isSolid(self.X-1, self.Y) 
	end

	function self.canMoveRight()
		return self.canMove() 
			and not self.hook.active 
			and self.X <SIZE_X-1 
			and not isSolid(self.X+1, self.Y) 
	end

	function self.canMoveDown()
		--if I use the grappling hook
		--but I don't use the laser
		--and there's nothing below my feet
		return self.canMove()
			and self.hook.active 
			and not isSolid(self.X, self.Y+1) 
	end

	function self.isFalling()
		return not isSolid(player.X, player.Y+1) and not self.hook.active
	end

	function self.turnOnLaser(direction)
		if not contains({'left', 'right'}, direction) then
			return
		end

		if player.isFalling() then
			return
		end
				
		self.laser.active = true
		self.laser.direction = direction
	end

	function self.turnOffLaser()
		self.laser.active = false
		self.spriteID = 1 
	end


	function self.displayGrapplingHook()
		if self.hook.active then
			tileLineWithCollision(map.Laser, GRAPPLING_HOOK, self.X, self.Y, 0, -1)
		end
	end

	function self.displayLaser()
		createLaserBeam(self.X, self.Y, self.laser.direction)
	end

	function self.draw()
		if SIZE_Y - self.Y >= yOffsetAtVictory then
			love.graphics.draw(
				PlayerspriteIDs, PlayerCharset[player.spriteID], tileToScreen(self.X, self.Y))
		end
	end

	function self.animate()
		if self.laser.active then
			if game_clock%15 == 0 then 
				if self.spriteID == 1 or self.spriteID == 5 then 
					self.spriteID = 3
				elseif self.spriteID == 3 or self.spriteID == 4 then 
					self.spriteID = self.spriteID + 1
				end
			end
		end
	end
	
	self._construct()
	return self
end