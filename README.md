# The Last Beacon

wip

## Credits

### Major contributors

#### Zoltán Schmidt

* **original [2014] version of the game**
* concept, game design
* coding
* level design
* artwork *(original only)*

#### Atis1

* artwork
    * 2019 redesign of the original graphics

### Contributors

### Used resources, code snippets, etc

* [Robert Machmer](https://github.com/rm-code/bresenham): Bresenham-algorithm, Lua implementation
