# To-do

A list of ideas that would significantly improve the engine.

## assets

* create "styles" for chapters and stuff
* better laser sound
* sound effect for destroying ruin blocks

## map format

* details on separate layers
    * details being tied to a base tile
* screw function calling and create a more universal format
    * potentially involve Tiled in the process

## editor

* extend saving function
    * "layout only": no decors


## bugs

* handle laser crossing (other laser; hook; prism/mirror behavior)
* damaged ruin blocks take the same time to disappear
* many more yet to be found and documented

## gameplay elements

* cannon
* gravity changing
    * directions
    * total off
* laser emitters
    * related: level doesn't end with activation
        => *exit door*
* multiple laser types
    * destructive, pass-thru, etc
* sticky blocks [if hook is used, it can't be unattached]
* slippery blocks [hook can't attach]
* hardened ruin [take time to burn through but impassable for player ]
* amplified mirror [works even when exit is blocked]
* force field 1: the firewall from Hackfield, kind of like in Portal 2
* force field 2: bends laser
* multi-screen levels, with conditional transition
* doors with reverse functionality (closing by default)

* level pass-codes, in old fashion