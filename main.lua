require '_src/load/assets'
require '_src/load/map'
require '_src/gui/draw'
require '_src/update/abilities'
require '_src/update/debug'
require '_src/control/keyboard'
require '_src/control/mouse'
require '_src/sound/play'


-- todo: for maps -- https://github.com/karai17/Simple-Tiled-Implementation
function love.load()
	math.randomseed( os.time() )

	require '_src/common/preload'

	loadSounds()		--sounds
	loadFont()			--initializing font
	loadTilesets()		--initializing various tiles
	loadPlayerAssets()	--initializing player sprites
	loadGraphics()		--initializing pics

	applySettings()

	map = Map()
end

function love.draw()
	-- main menu
	if GLOBAL_STATE == "MENU" then
		if menu == SUB_MENU.MAIN then
			drawMainMenu()
		elseif menu == SUB_MENU.CREDITS then
			drawCredits()
		end	
	end

	drawBackground()

	-- game
	if contains({"EDITOR", "MAP", "PHASE_OUT", "ENDING"}, GLOBAL_STATE) then
		map.drawGameplay()

		--[[
			LAYER 5: GUI, ETC
		]]--
		drawHUD()
		--for the case of victory	
		if map.isFinished() then
			drawBeaconArtifacts()

			if GLOBAL_STATE == "ENDING" then
				love.graphics.print(endingText, OFFSET_X+16 ,HEIGHT - ((yOffsetAtVictory - 3)*TILE_SIZE))			
			end

			if map.id ~= EDITOR_MAP then
				printRelative("Beacon\n"..map.id.." active",6,49) 			
			end
		end
	end

	-- debug
	love.graphics.print("GLOBAL_STATE: " .. GLOBAL_STATE .. "\nMAP STATE: " .. map.state, 0, 0)
end

function love.update(dt)
    game_clock = game_clock+1
    
    updateMouse()

	--beginning sequence	
	if GLOBAL_STATE == "INTRO" then
		if yOffsetAtStart ~= 0 then 
			yOffsetAtStart = yOffsetAtStart + 1
		else
			if k == 0 then
				love.audio.play(menuSound)
				k = k+1
			end
		end
	end

	if contains({"EDITOR", "MAP", "PHASE_OUT", "ENDING"}, GLOBAL_STATE) then
        map.updateGameplay(dt)
	end
end