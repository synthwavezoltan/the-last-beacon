--[[
    GAME-SPECIFIC GLOBAL CONSTANTS
]]--

-- tile shortcuts
ROCK = 1
ROCK_BG = 2
RUIN_START = 3
RUIN_END = 8

MIRROR = {
    LD = 9,
    LU = 10,
    RU = 11,
    RD = 12  
}

PRISM = {
    D = 13,
    L = 14,
    U = 15,
    R = 16
}

EXIT = 17

LOCK = {
    CLOSED = 19,
    OPEN = 20
}

DOOR = {
    CLOSED = 21,
    OPEN = 22
}

PORTAL = {
    PH1 = 25,
    PH2 = 26
}

ROCK_BEAM = {
    -- horizontal
    L = 30,
    H = 31,
    R = 32,
    -- vertical
    U = 54,
    V = 55,
    D = 56,
}


FEATURED_ON_MAP = {
    RUINS = 1,
    MIRROR = 2,
    PRISM = 3,
    DOOR = 4,
    PORTAL = 5,
}

ACTIVE_ABILITIES = {
    HOOK = 11,
    LASER = 12,
}

LASER_VER = 61
LASER_HOR = 62
LASER_CROSS = 60

GRAPPLING_HOOK = 63

EMPTY = 64
