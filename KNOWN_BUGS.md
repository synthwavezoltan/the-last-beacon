# Known bugs

Consider it a to-do list.

* Portals can receive only one laser beam

* Mirrors create a laser beam back to the player's direction

* Laser going through rope does not "cut" it

* Exit Beacon animation broken

* Intro is broken: missing text, texture, no sound

* The last map is impossible to beat due to an unknown bug, most likely somewhere in level design